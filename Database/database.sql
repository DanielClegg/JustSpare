USE [master]
GO
/****** Object:  Database [JustSpare]    Script Date: 01/07/2017 19:47:20 ******/
CREATE DATABASE [JustSpare]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'JustSpare', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\JustSpare.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'JustSpare_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\JustSpare_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [JustSpare] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [JustSpare].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [JustSpare] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [JustSpare] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [JustSpare] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [JustSpare] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [JustSpare] SET ARITHABORT OFF 
GO
ALTER DATABASE [JustSpare] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [JustSpare] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [JustSpare] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [JustSpare] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [JustSpare] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [JustSpare] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [JustSpare] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [JustSpare] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [JustSpare] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [JustSpare] SET  DISABLE_BROKER 
GO
ALTER DATABASE [JustSpare] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [JustSpare] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [JustSpare] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [JustSpare] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [JustSpare] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [JustSpare] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [JustSpare] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [JustSpare] SET RECOVERY FULL 
GO
ALTER DATABASE [JustSpare] SET  MULTI_USER 
GO
ALTER DATABASE [JustSpare] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [JustSpare] SET DB_CHAINING OFF 
GO
ALTER DATABASE [JustSpare] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [JustSpare] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [JustSpare] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'JustSpare', N'ON'
GO
ALTER DATABASE [JustSpare] SET QUERY_STORE = OFF
GO
USE [JustSpare]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [JustSpare]
GO
/****** Object:  Table [dbo].[Claim]    Script Date: 01/07/2017 19:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Claim](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ClaimDate] [date] NOT NULL,
 CONSTRAINT [PK_Claim] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ClaimedItems]    Script Date: 01/07/2017 19:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ClaimedItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ItemId] [int] NOT NULL,
	[ClaimId] [int] NOT NULL,
 CONSTRAINT [PK_ClaimItems] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Item]    Script Date: 01/07/2017 19:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Item](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[NumberOf] [int] NOT NULL,
	[DonatedByUserId] [int] NOT NULL,
	[Description] [nvarchar](500) NOT NULL,
	[DonatedDate] [date] NOT NULL,
	[QualityType] [int] NOT NULL,
	[ItemType] [int] NOT NULL,
	[Claimed] [bit] NULL,
 CONSTRAINT [PK_Item] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 01/07/2017 19:47:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](250) NOT NULL,
	[Address] [nvarchar](50) NOT NULL,
	[Postcode] [nvarchar](50) NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[UpdatedDate] [date] NOT NULL,
	[LastLoggedInDate] [date] NOT NULL,
	[Email] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[ContactName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ClaimedItems]  WITH CHECK ADD  CONSTRAINT [FK_ClaimItems_Claim] FOREIGN KEY([ClaimId])
REFERENCES [dbo].[Claim] ([Id])
GO
ALTER TABLE [dbo].[ClaimedItems] CHECK CONSTRAINT [FK_ClaimItems_Claim]
GO
ALTER TABLE [dbo].[ClaimedItems]  WITH CHECK ADD  CONSTRAINT [FK_ClaimItems_Item] FOREIGN KEY([ItemId])
REFERENCES [dbo].[Item] ([Id])
GO
ALTER TABLE [dbo].[ClaimedItems] CHECK CONSTRAINT [FK_ClaimItems_Item]
GO
USE [master]
GO
ALTER DATABASE [JustSpare] SET  READ_WRITE 
GO
