﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dal
{
    public class Enums
    {
        public enum QualityType
        {
            Used,
            New,
            Fresh
        }

        public enum ItemType
        {
            Food,
            Clothing,
            Medical,
            Hygiene
        }
    }
}
