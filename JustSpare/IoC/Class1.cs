﻿using Repository;
using StructureMap;

namespace Inversion
{
    public static class Register
    {
        private static Container _container;

        public static Container GetContainer()
        {
            if (_container == null)
            {
                _container = new Container();

                Configure();
            }

            return _container;
        }

        private static void Configure()
        {
            _container.Configure(c => { c.For(typeof(IGenericRepository<>)).Use(typeof(GenericRepository<>)); });
        }
    }
}