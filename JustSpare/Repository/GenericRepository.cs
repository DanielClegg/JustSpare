﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dal;

namespace Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private readonly JustSpareEntities _dbContext;

        public GenericRepository(JustSpareEntities dbContext)
        {
            _dbContext = dbContext;
        }
        public  void Update()
        {
            _dbContext.SaveChanges();
        }

        public T Find(int id)
        {
            var set = _dbContext.Set<T>();

            return set.Find(id);
        }

        public T Find(Func<T, bool> query)
        {
            var set = _dbContext.Set<T>();

            return set.Where(query).FirstOrDefault();
        }

        public T Add(T obj)
        {
            var set = _dbContext.Set<T>();

            var newObj = set.Add(obj);

            _dbContext.SaveChanges();

            return newObj;
        }

        public IEnumerable<T> List(Func<T, bool> query)
        {
            var set = _dbContext.Set<T>();

            return set.Where(query);
        }

        public List<T> List()
        {
            var set = _dbContext.Set<T>();

            var results = set.ToList();

            return results;
        }

        public void Delete(T obj)
        {
            var set = _dbContext.Set<T>();

            set.Remove(obj);

            _dbContext.SaveChanges();
        }
    }
}