﻿using System;
using Dal;
using Repository;

namespace Service
{
    public class AccountService
    {
        private readonly IGenericRepository<User> _userRepository;

        public AccountService(IGenericRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public User GetUserById(int id)
        {
            var user = _userRepository.Find(id);

            return user;
        }

        public User GetUserByName(string username)
        {
            var user = _userRepository.Find(c => c.Username == username);

            return user;
        }

        public User RegisterUser(string address, string contactName, string email, string password, string phoneNumber,
            string postcode, string userName)
        {
            if (_userRepository.Find(c => c.Username == userName) != null)
                throw new Exceptions.UserAlreadyExistsException();

            var newUser = new User
            {
                Address = address,
                CreatedDate = DateTime.UtcNow,
                LastLoggedInDate = DateTime.MinValue,
                Password = SecurePasswordHasher.Hash(password),
                Postcode = postcode,
                UpdatedDate = DateTime.UtcNow,
                Username = userName,
                ContactName = contactName,
                Email = email,
                PhoneNumber = phoneNumber
            };
            return _userRepository.Add(newUser);
        }

        public bool ValidateUser(string username, string password)
        {
            bool result = false;

            var user = _userRepository.Find(c =>  c.Username == username);

            if (user != null)        
                result = SecurePasswordHasher.Verify(password, user.Password);

            return result;
        }
    }
}