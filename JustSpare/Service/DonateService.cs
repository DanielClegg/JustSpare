﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dal;
using Repository;

namespace Service
{
    public class DonateService
    {
        private readonly IGenericRepository<Item> _itemRepository;
        private readonly IGenericRepository<User> _userRepository;
        private readonly AccountService _accountService;

        public DonateService(IGenericRepository<Item> itemRepository, IGenericRepository<User> userRepository, AccountService accountService)
        {
            _itemRepository = itemRepository;
            _userRepository = userRepository;
            _accountService = accountService;
        }

        public Item DonateItem(string description, Enums.ItemType itemType, string name, int numberOf, Enums.QualityType qualityType, string username)
        {
            var newItem = new Item()
            {
                Description = description,
                DonatedByUserId = _accountService.GetUserByName(username).Id,
                DonatedDate = DateTime.UtcNow,
                Name = name,
                NumberOf = numberOf,
                QualityType = Convert.ToInt32( qualityType),
                ItemType = Convert.ToInt32(itemType)
            };

            _itemRepository.Add(newItem);

            return newItem;
        }
    }
}
