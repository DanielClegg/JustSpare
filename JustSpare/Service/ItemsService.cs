﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dal;
using Repository;

namespace Service
{
    public class ItemsService
    {
        private readonly IGenericRepository<ClaimedItem> _claimedItemRepository;
        private readonly IGenericRepository<Claim> _claimRepository;
        private readonly IGenericRepository<Item> _itemRepository;
        private readonly IGenericRepository<User> _userRepository;

        public ItemsService(IGenericRepository<Item> itemRepository, IGenericRepository<User> userRepository,
            IGenericRepository<Claim> claimRepository, IGenericRepository<ClaimedItem> claimedItemRepository)
        {
            _itemRepository = itemRepository;
            _userRepository = userRepository;
            _claimRepository = claimRepository;
            _claimedItemRepository = claimedItemRepository;
        }

        public void CreateClaim(int userId, List<int> claimedIds)
        {
            var claim = new Claim {UserId = userId, ClaimDate = DateTime.UtcNow};

            var newClaim = _claimRepository.Add(claim);

            foreach (var id in claimedIds)
            {
                var item = _itemRepository.Find(id);
                item.Claimed = true;
                _itemRepository.Update();

                _claimedItemRepository.Add(new ClaimedItem
                {
                    ClaimId = newClaim.Id,
                    ItemId = id
                });
            }
        }

        public List<Item> GetAllItems()
        {
            var list = _itemRepository.List();

            return list;
        }

        public List<Item> GetAllUnclaimedItems()
        {
            var list = _itemRepository.List().Where(c => c.Claimed == false).ToList();

            return list;
        }

        public List<Item> GetClaimedItems(int userId)
        {
            var listOfClaimedItems = new List<Item>();

            var claim = _claimRepository.Find(c => c.UserId == userId);

            if (claim != null)
                foreach (var claimClaimedItem in claim.ClaimedItems)
                    listOfClaimedItems.Add(claimClaimedItem.Item);
            return listOfClaimedItems;
        }

        public List<Item> GetUserItems(string name)
        {
            var userId = _userRepository.Find(c => c.Username == name).Id;

            var list = GetAllItems().Where(c => c.DonatedByUserId == userId).ToList();

            return list;
        }
    }
}