﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Security;
using Dal;
using Service;
using Web.Models;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly AccountService _accountService;
        private readonly ItemsService _itemsService;

        public HomeController(AccountService accountService, ItemsService itemsService)
        {
            _accountService = accountService;
            _itemsService = itemsService;
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            var allItems = _itemsService.GetAllItems();

            var donateViewModels = new List<DonateViewModel>();
            foreach (var item in allItems)
                donateViewModels.Add(new DonateViewModel
                {
                    Description = item.Description,
                    DonatedBy = _accountService.GetUserById(item.DonatedByUserId).ContactName,
                    ItemType = (Enums.ItemType) Enum.Parse(typeof(Enums.ItemType), item.ItemType.ToString()),
                    Name = item.Name,
                    NumberOf = item.NumberOf,
                    QualityType =
                        (Enums.QualityType) Enum.Parse(typeof(Enums.QualityType), item.QualityType.ToString()),
                    DonatedDate = item.DonatedDate
                });

            return View(donateViewModels);
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LearnMore()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel loginViewModel)
        {
            if (!ModelState.IsValid)
                return View("Login", loginViewModel);


            if (!_accountService.ValidateUser(loginViewModel.Username, loginViewModel.Password))
                return View("Login", loginViewModel);

            FormsAuthentication.SetAuthCookie(loginViewModel.Username, false);

            return RedirectToAction("Dashboard");
        }

        public ActionResult Login()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Register(RegisterViewModel registerViewModel)
        {
            if (!ModelState.IsValid)
                return View("Register", registerViewModel);

            try
            {
                _accountService.RegisterUser(registerViewModel.Address, registerViewModel.ContactName,
                    registerViewModel.Email, registerViewModel.Password, registerViewModel.PhoneNumber,
                    registerViewModel.Postcode, registerViewModel.UserName);
            }
            catch (Exceptions.UserAlreadyExistsException e)
            {
                ModelState.AddModelError("UserName", "User already exists!");
                return View("Register", registerViewModel);
            }

            return RedirectToAction("Login");
        }

        public ActionResult Register()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Registered()
        {
            return View();
        }
    }
}