﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Dal;
using Service;
using Web.Models;

namespace Web.Controllers
{
    public class ItemsController : Controller
    {
        private readonly AccountService _accountService;
        private readonly DonateService _donateService;
        private readonly ItemsService _itemsService;

        private int _userId;

        public ItemsController(DonateService donateService, AccountService accountService, ItemsService itemsService)
        {
            _donateService = donateService;
            _accountService = accountService;
            _itemsService = itemsService;
        }

        public ActionResult DonateItems()
        {
            return View();
        }

        [HttpPost]
        public ActionResult DonateItems(DonateViewModel donateViewModel)
        {
            if (!ModelState.IsValid)
                return View("DonateItems", donateViewModel);

            var username = User.Identity.Name;

            _donateService.DonateItem(donateViewModel.Description, donateViewModel.ItemType, donateViewModel.Name,
                donateViewModel.NumberOf, donateViewModel.QualityType, username);

            return RedirectToAction("UserListOfItems");
        }

        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult ItemsClaimed()
        {
            _userId = _accountService.GetUserByName(User.Identity.Name).Id;
            var itemsClaimed = _itemsService.GetClaimedItems(_userId);

            var donateViewModels = GenerateDonateViewModels(itemsClaimed);

            return View(donateViewModels);
        }

        [HttpPost]
        public ActionResult ListOfItems(List<DonateViewModel> claimedItems)
        {
            _userId = _accountService.GetUserByName(User.Identity.Name).Id;
            var claimedIds = new List<int>();
            foreach (var item in claimedItems)
                if (item.ItemSelected)
                    claimedIds.Add(item.Id);

            _itemsService.CreateClaim(_userId, claimedIds);

            return RedirectToAction("ItemsClaimed");
        }

        public ActionResult ListOfItems()
        {
            var allItems = _itemsService.GetAllUnclaimedItems();

            var donateViewModels = GenerateDonateViewModels(allItems);

            return View(donateViewModels);
        }

        public ActionResult UserListOfItems()
        {
            var allItems = _itemsService.GetUserItems(User.Identity.Name);

            var donateViewModels = GenerateDonateViewModels(allItems);

            return View(donateViewModels);
        }

        private List<DonateViewModel> GenerateDonateViewModels(List<Item> itemsClaimed)
        {
            var donateViewModels = new List<DonateViewModel>();
            foreach (var item in itemsClaimed)
                donateViewModels.Add(new DonateViewModel
                {
                    Description = item.Description,
                    DonatedBy = _accountService.GetUserById(item.DonatedByUserId).ContactName,
                    ItemType = (Enums.ItemType) Enum.Parse(typeof(Enums.ItemType), item.ItemType.ToString()),
                    Name = item.Name,
                    NumberOf = item.NumberOf,
                    QualityType =
                        (Enums.QualityType) Enum.Parse(typeof(Enums.QualityType), item.QualityType.ToString()),
                    Id = item.Id
                });
            return donateViewModels;
        }
    }
}