﻿using System;
using System.ComponentModel.DataAnnotations;
using Dal;

namespace Web.Models
{
    public class DonateViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int NumberOf { get; set; }

        [Required]
        public string Description { get; set; }

        public Enums.QualityType QualityType { get; set; }
        public Enums.ItemType ItemType { get; set; }

        public string DonatedBy { get; set; }

        public DateTime DonatedDate { get; set; }

        public bool ItemSelected { get; set; }

        public int Id { get; set; }
    }
}

