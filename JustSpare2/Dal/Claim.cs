﻿using System;
using System.Collections.Generic;

namespace Dal
{
    public class Claim
    {
        public Claim()
        {
            this.ClaimedItems = new List<ClaimedItem>();
        }

        public int Id { get; set; }
        public int UserId { get; set; }
        public System.DateTime ClaimDate { get; set; }

        public virtual ICollection<ClaimedItem> ClaimedItems { get; set; }
    }
}
