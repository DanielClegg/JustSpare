namespace Dal
{
    public partial class ClaimedItem
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int ClaimId { get; set; }

        public virtual Claim Claim { get; set; }
        public virtual Item Item { get; set; }
    }
}