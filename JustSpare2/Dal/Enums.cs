namespace Dal
{
    public class Enums
    {
        public enum QualityType
        {
            Used,
            New,
            Fresh
        }

        public enum ItemType
        {
            Food,
            Clothing,
            Medical,
            Hygiene
        }
    }
}