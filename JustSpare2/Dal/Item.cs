using System;
using System.Collections.Generic;

namespace Dal
{
    public class Item
    {
        public Item()
        {
            this.ClaimedItems = new List<ClaimedItem>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOf { get; set; }
        public int DonatedByUserId { get; set; }
        public string Description { get; set; }
        public System.DateTime DonatedDate { get; set; }
        public int QualityType { get; set; }
        public int ItemType { get; set; }
        public Nullable<bool> Claimed { get; set; }

        public virtual ICollection<ClaimedItem> ClaimedItems { get; set; }
    }
}