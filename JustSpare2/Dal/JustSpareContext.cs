using System.IO;
using Microsoft.EntityFrameworkCore;

namespace Dal
{
    public class JustSpareContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<ClaimedItem> ClaimedItems { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Claim> Claims { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string path = Path.Combine(System.Environment.CurrentDirectory, "JustSpare.db");
            optionsBuilder.UseSqlite($"Filename={path}");
        }
    }
}