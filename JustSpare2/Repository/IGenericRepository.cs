using System;
using System.Collections.Generic;

namespace Repository
{
    public interface IGenericRepository<T> where T : class
    {
        T Find(int id);
        T Find(Func<T, bool> query);
        T Add(T obj);
        List<T> List();
        IEnumerable<T> List(Func<T, bool> query);
        void Delete(T obj);
        void Update();
    }
}